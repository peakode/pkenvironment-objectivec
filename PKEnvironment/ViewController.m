//
//  ViewController.m
//  PKEnvironment
//
//  Created by Gokhan Gultekin on 16/05/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

#import "ViewController.h"
#import "Config.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString * text = @"";
    
    /*
     Gokhan:
     You can use switch-case structure below. It allows you to manage environments easily.
     */
    
    switch ([Config environment]) {
        case Development:
            text = @"DEVELOPMENT";
            break;
        case Test:
            text = @"TEST";
            break;
        case AppStore:
            text = @"APP STORE";
            break;
        default:
            break;
    }
    
    PKLog(@"Logs won't be shown when enviroment is App Store!");
    
    envLabel.text = text;
    
}

@end
