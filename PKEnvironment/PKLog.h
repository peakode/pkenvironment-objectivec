//
//  PKLog.h
//  PKEnvironment
//
//  Created by Gokhan Gultekin on 26/05/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

#ifndef PKLog_h
#define PKLog_h

#ifdef APPSTORE
#define PKLog(...)

#endif

#ifdef DEVELOPMENT

#define PKLog(...) NSLog(@"\n\nMeta:\nEnvironment: Development\n\nLog:\n%@",__VA_ARGS__)

#endif

#ifdef TEST

#define PKLog(...) NSLog(@"\n\n**Meta:\nEnvironment: Test\n\n**Log:\n%@",__VA_ARGS__)

#endif /* PKLog_h */
#endif
