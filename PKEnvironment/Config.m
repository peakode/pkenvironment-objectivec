//
//  Config.m
//  PKEnvironment
//
//  Created by Gokhan Gultekin on 26/05/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

#import "Config.h"
#import "PKLog.h"

@implementation Config

+ (Environment) environment {
    
    Environment env = Development;
    
    #ifdef APPSTORE
        env = AppStore;
    #endif
        
    #ifdef DEVELOPMENT
        env = Development;
    #endif
        
    #ifdef TEST
        env = Test;
    #endif

    return env;
}


+ (NSString*) baseUrl {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"PKBaseUrl"];
}

@end
