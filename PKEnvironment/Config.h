//
//  Config.h
//  PKEnvironment
//
//  Created by Gokhan Gultekin on 26/05/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKLog.h"

typedef enum : NSUInteger {
    Development,
    Test,
    AppStore,
} Environment;

@interface Config : NSObject

+ (Environment) environment;

+ (NSString*) baseUrl;

@end
