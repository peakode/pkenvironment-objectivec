//
//  Confing.m
//  PKEnvironment
//
//  Created by Gokhan Gultekin on 26/05/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

#import "Confing.h"

@implementation Confing

+ (Environment) ENVIRONMENT {
    
    Environment env = Development;
    
    #ifdef APPSTORE
        env = AppStore;
    #endif

    #ifdef DEVELOPMENT
        env = Development;
    #endif

    #ifdef TEST
        env = Test;
    #endif
    
    return env;

}

+ (NSString*) BASE_URL {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"PKBaseUrl"];
}

@end
