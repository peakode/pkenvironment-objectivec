//
//  AppDelegate.h
//  PKEnvironment
//
//  Created by Gokhan Gultekin on 16/05/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

