//
//  main.m
//  PKEnvironment
//
//  Created by Gokhan Gultekin on 16/05/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
